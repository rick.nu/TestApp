import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders basic application', () => {
    render(<App />);

    const linkElement = screen.getByText(/Kompany/i);

    expect(linkElement).toBeInTheDocument();
});
