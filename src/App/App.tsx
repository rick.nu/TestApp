import { ReactElement } from 'react';
import Header from './Layout/Header';
import Companies from './Companies';
import { GlobalStyle } from './App.style';

const App = (): ReactElement => (
    <>
        <GlobalStyle />
        <Header />
        <Companies />
    </>
);

export default App;
