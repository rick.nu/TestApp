import { ReactElement } from 'react';
import { Company } from '../../../types/company';
import { CompanyItem, Logo, Details, Name, MoreDetails } from './CompanyList.style';

type Props = {
    companies: Company[];
    onDetailClick: (id: string) => void;
};

const CompanyList = ({ companies, onDetailClick }: Props): ReactElement => {
    return (
        <div>
            {companies.map((company) => (
                <CompanyItem key={company.id}>
                    <Logo src={company.logo} alt={company.name} />
                    <Details>
                        <Name>{company.name}</Name>
                        {company.streetName}, {company.zipCode} {company.city}
                    </Details>
                    <MoreDetails onClick={() => onDetailClick(company.id)}>Details</MoreDetails>
                </CompanyItem>
            ))}
        </div>
    );
};

export default CompanyList;
