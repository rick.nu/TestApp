import { ReactElement, useEffect, useState } from 'react';
import Container from '../Layout/Container';
import Section from '../Layout/Section';
import SearchBar from '../../components/SearchBar';
import CompanyList from './CompanyList';
import { Company } from '../../types/company';
import { getCompanies } from '../../service/API';
import Alert from '../../components/Alert';
import CompanyDetails from './CompanyDetails';

const Companies = (): ReactElement => {
    const [companies, setCompanies] = useState<Company[]>([]);
    const [search, setSearch] = useState<string | null>(null);
    const [alert, setAlert] = useState<ReactElement | null>(null);
    const [detailView, setDetailView] = useState<string | null>(null);

    useEffect(() => {
        const loadCompanies = async () => {
            try {
                const loadedCompanies = await getCompanies(search);

                setCompanies(loadedCompanies);
            } catch (error) {
                setAlert(<Alert>De data kon niet worden geladen, probeer het later nog eens.</Alert>);
            }
        };

        loadCompanies();
    }, [search]);

    const findCompany = (id: string) => companies.find((company) => company.id === id);

    return (
        <Section>
            <Container>
                <SearchBar onSearch={(query) => setSearch(query)} />
                {alert}
                <CompanyList companies={companies} onDetailClick={(id) => setDetailView(id)} />
                {detailView && (
                    <CompanyDetails
                        id={detailView}
                        company={findCompany(detailView)}
                        onClose={() => setDetailView(null)}
                    />
                )}
            </Container>
        </Section>
    );
};

export default Companies;
