import { ReactElement, ReactNode } from 'react';
import styled from 'styled-components';

type Props = {
    children: ReactNode;
};

const FixedWidth = styled.div`
    padding: 0 1rem;
    max-width: 50rem;
    margin: 0 auto;
`;

const Container = ({ children }: Props): ReactElement => {
    return <FixedWidth>{children}</FixedWidth>;
};

export default Container;
