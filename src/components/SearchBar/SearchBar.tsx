import { FormEvent, ReactElement, useState } from 'react';
import { Button, Input, Form } from './SearchBar.style';

type Props = {
    onSearch: (query: string) => void;
};

const SearchBar = ({ onSearch }: Props): ReactElement => {
    const [query, setQuery] = useState<string>('');

    const handleSearch = (event: FormEvent) => {
        event.preventDefault();

        onSearch(query);
    };

    const onInputChange = (event: FormEvent<HTMLInputElement>) => setQuery(event.currentTarget.value);

    return (
        <Form onSubmit={handleSearch}>
            <Input onChange={onInputChange} type="text" />
            <Button type="submit" aria-label="Zoeken">
                Zoeken
            </Button>
        </Form>
    );
};

export default SearchBar;
