import { ReactElement, ReactNode } from 'react';
import styled from 'styled-components';

type Props = {
    children: ReactNode;
};

const AlertBox = styled.div`
    margin-bottom: 2rem;
    padding: 1rem;
    border-radius: 0.3rem;
    background: #d00000;
`;

const Alert = ({ children }: Props): ReactElement => {
    return <AlertBox>{children}</AlertBox>;
};

export default Alert;
